#!/usr/bin/python
import ldns
import sys
import threading
import os

debug = True

class CheckDns(object):
    def __init__(self, security_type, forwarder, hostname):
        self.security_type = security_type
        self.forwarder = forwarder
        self.hostname = hostname
        self.anchor = list()
        self.root = None
        self.answers = list()

        if security_type == 'DNS_SECURITY_DNSSEC':
            self.check_dnssec()

        elif security_type == 'DNS_SECURITY_DNSCRYPT':
            self.check_dnscrypt()

        elif security_type == 'DNS_SECURITY_NONE':
            self.forwarder = None
            self.check_dns()

    def root_anchors(self):
        # self.anchor.append(".			172800	IN	DNSKEY	256 3 8 AwEAAcH+axCdUOsTc9o+jmyVq5rsGTh1EcatSumPqEfsPBT+whyj0/UhD7cWeixV9Wqzj/cnqs8iWELqhdzGX41ZtaNQUfWNfOriASnWmX2D9m/EunplHu8nMSlDnDcT7+llE9tjk5HI1Sr7d9N16ZTIrbVALf65VB2ABbBG39dyAb7tz21PICJbSp2cd77UF7NFqEVkqohl/LkDw+7Apalmp0qAQT1Mgwi2cVxZMKUiciA6EqS+KNajf0A6olO2oEhZnGGY6b1LTg34/YfHdiIIZQqAfqbieruCGHRiSscC2ZE7iNreL/76f4JyIEUNkt6bQA29JsegxorLzQkpF7NKqZc=")
        #self.anchor.append(".			172800	IN	DNSKEY	257 3 8 AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3+/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kvArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+eoZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfdRUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwNR1AkUTV74bU=")
        #self.anchor.append(".			172800	IN	DNSKEY	385 3 8 AwEAAagAIKlVZrpC6Ia7gEzahOR+9W29euxhJhVVLOyQbSEW0O8gcCjFFVQUTf6v58fLjwBd0YI0EzrAcQqBGCzh/RStIoO8g0NfnfL2MTJRkxoXbfDaUeVPQuYEhg37NZWAJQ9VnMVDxP/VHL496M/QZxkjf5/Efucp2gaDX6RS6CXpoY68LsvPVjR0ZSwzz1apAzvN9dlzEheX7ICJBBtuA6G3LQpzW5hOA2hzCTMjJPJ8LbqF6dsV6DoBQzgul0sGIcGOYl7OyQdXfZ57relSQageu+ipAdTTJ25AsRTAoub8ONGcLmqrAmRLKBP1dfwhYB4N7knNnulqQxA+Uk1ihz0=")
        self.root = ".			172800	IN	DNSKEY	256 3 8 AwEAAcH+axCdUOsTc9o+jmyVq5rsGTh1EcatSumPqEfsPBT+whyj0/UhD7cWeixV9Wqzj/cnqs8iWELqhdzGX41ZtaNQUfWNfOriASnWmX2D9m/EunplHu8nMSlDnDcT7+llE9tjk5HI1Sr7d9N16ZTIrbVALf65VB2ABbBG39dyAb7tz21PICJbSp2cd77UF7NFqEVkqohl/LkDw+7Apalmp0qAQT1Mgwi2cVxZMKUiciA6EqS+KNajf0A6olO2oEhZnGGY6b1LTg34/YfHdiIIZQqAfqbieruCGHRiSscC2ZE7iNreL/76f4JyIEUNkt6bQA29JsegxorLzQkpF7NKqZc="
        return self.root

    def create_new_chain(self, response):
        data_chain_orig = ldns.ldns_dnssec_data_chain_new()
        data_chain = ldns.ldns_dnssec_build_data_chain(self.dnssec_resolver, qflags=0, data_set=None,
                                                       pkt=response, orig_rr=self.anchors[0])
        return data_chain

    def check_dnssec(self):
        debug = True
        dns_root_anchor = self.root_anchors()
        if os.path.exists("/etc/resolv.conf"):
            self.dnssec_resolver = ldns.ldns_resolver.new_frm_file("/etc/resolv.conf")
        self.dnssec_resolver.set_dnssec(True)

        # for anchor in dns_root_anchor:
        temp_anchor = ldns.ldns_rr.new_frm_str(".			172800	IN	DNSKEY	256 3 8 AwEAAcH+axCdUOsTc9o+jmyVq5rsGTh1EcatSumPqEfsPBT+whyj0/UhD7cWeixV9Wqzj/cnqs8iWELqhdzGX41ZtaNQUfWNfOriASnWmX2D9m/EunplHu8nMSlDnDcT7+llE9tjk5HI1Sr7d9N16ZTIrbVALf65VB2ABbBG39dyAb7tz21PICJbSp2cd77UF7NFqEVkqohl/LkDw+7Apalmp0qAQT1Mgwi2cVxZMKUiciA6EqS+KNajf0A6olO2oEhZnGGY6b1LTg34/YfHdiIIZQqAfqbieruCGHRiSscC2ZE7iNreL/76f4JyIEUNkt6bQA29JsegxorLzQkpF7NKqZc=")
        self.dnssec_resolver.push_dnssec_anchor(temp_anchor)

        self.dns_forwarder = ldns.ldns_rdf.new_frm_str(self.forwarder, ldns.LDNS_RDF_TYPE_A)
        self.dnssec_resolver.push_nameserver(self.dns_forwarder)

        dnssec_response = self.dnssec_resolver.query(self.hostname, ldns.LDNS_RR_TYPE_A, ldns.LDNS_RR_CLASS_IN)

        # try:
        #     self.trust_chain = self.create_new_chain(dnssec_response)
        # except Exception:
        #     del self.trust_chain
        #     pass

        if dnssec_response and dnssec_response.answer():

            rrs = dnssec_response.answer()
            while True:
                popped_val = rrs.pop_rr()
                if popped_val is not None:
                    self.answers.append(str(popped_val.a_address()))
                else:
                    break

            # SERVFAIL indicated bogus name
            if dnssec_response.get_rcode() is ldns.LDNS_RCODE_SERVFAIL:
                print( self.hostname, "is bogus")

            # Check AD (Authenticated) bit
            if dnssec_response.get_rcode() is ldns.LDNS_RCODE_NOERROR:
                if dnssec_response.ad():
                    return self.answers
                else:
                    self.no_ip = list()
                    self.no_ip.append(None)
                    print(self.no_ip)
                    return self.no_ip

    def check_dnscrypt(self):
        pass

    def check_dns(self):
        if os.path.exists("/etc/resolv.conf"):
            self.dns_resolver = ldns.ldns_resolver.new_frm_file("/etc/resolv.conf")
        self.dns_resolver.set_dnssec(False)
        dns_response = self.dns_resolver.query(self.hostname, ldns.LDNS_RR_TYPE_A, ldns.LDNS_RR_CLASS_IN)
        if dns_response and dns_response.answer():

            rrs = dns_response.answer()
            while True:
                popped_val = rrs.pop_rr()
                if popped_val is not None:
                    self.answers.append(str(popped_val.a_address()))
                else:
                    break

        if dns_response.get_rcode() is ldns.LDNS_RCODE_SERVFAIL:
            self.no_ip = list()
            self.no_ip.append(None)
            return self.no_ip

        if dns_response.get_rcode() is ldns.LDNS_RCODE_NOERROR:
            if dns_response.ad():
                return self.answers


def main(security_type, forwarder, hostname):
    dns = CheckDns(hostname= 'www.yahoo.com' ,security_type = 'DNS_SECURITY_DNSSEC', forwarder = '8.8.8.8' )
    dns.check_dnssec()


if __name__ == '__main__':
    main(security_type=None, forwarder=None, hostname=None)


